import React from "react";
import FoodList from "./FoodList";
import { foodData } from "../data/food";
import "../css/Food.css";

export default function Food({ foods }) {
  const { id, img, title, price, info } = foods;
  return (
    <div className="card cardFood col-xs-12 col-sm-6 col-md-4 col-lg-3">
      <img className="card-img-top" src={img} alt="Card image cap" />
      <div className="card-body">
        <h5>{title}</h5>
        <p>
          <strong>{price}rsd</strong>
        </p>
        <p>
          <button className="btn btn-warning">Dodaj u korpu</button>
        </p>
      </div>
    </div>
  );
}
