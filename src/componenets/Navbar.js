import React from "react";
import { Link } from "react-router-dom";
import logo from "../Logo.png";
import styles from "../css/Navbar.module.css";

export default function Navbar() {
  return (
    <nav className={styles.bgColor + " navbar navbar-expand-md navbar-dark"}>
      <span className="navbar-brand" href="#">
        <Link to="/">
          <img
            src={logo}
            alt="store"
            className={styles.logo + " navbar-brand"}
          />
        </Link>
      </span>

      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#collapsibleNavbar"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="collapsibleNavbar">
        <ul className="navbar-nav">
          <li className={styles.navItem + " nav-item"}>
            <Link to="/">
              <p>Početna</p>
            </Link>
          </li>
          <li className={styles.navItem + " nav-item"}>
            <Link to="/foodList">
              <p>Hrana</p>
            </Link>
          </li>
          <li className={styles.navItem + " nav-item"}>
            <Link to="/drink">
              <p>Piće</p>
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}
