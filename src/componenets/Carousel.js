import React from "react";
import "../css/Carousel.css";

export default function Carousel() {
  return (
    <div
      id="carousel-example-1z"
      className="carousel slide carousel-fade"
      data-ride="carousel"
    >
      <ol className="carousel-indicators">
        <li
          data-target="#carousel-example-1z"
          data-slide-to="0"
          className="active"
        ></li>
        <li data-target="#carousel-example-1z" data-slide-to="1"></li>
        <li data-target="#carousel-example-1z" data-slide-to="2"></li>
      </ol>
      <div className="carousel-inner" role="listbox">
        <div className="carousel-item active">
          <img
            className="d-block w-100"
            src="https://www.monument.rs/images/slider/restoran-monument-big-fashion-trzni-centar-01.jpg"
            alt="First slide"
          />
        </div>
        <div className="carousel-item">
          <img
            className="d-block w-100"
            src="https://www.monument.rs/images/slider/restoran-monument-big-fashion-trzni-centar-03.jpg"
            alt="Second slide"
          />
        </div>
        <div className="carousel-item">
          <img
            className="d-block w-100"
            src="https://www.monument.rs/images/slider/restoran-monument-beograd-kafe.jpg"
            alt="Third slide"
          />
        </div>
      </div>
      <div
        className="carousel-control-prev"
        href="#carousel-example-1z"
        role="button"
        data-slide="prev"
      >
        <span
          className="moveControl carousel-control-prev-icon"
          aria-hidden="true"
        ></span>
        <span className="sr-only">Previous</span>
      </div>
      <div
        className="carousel-control-next"
        href="#carousel-example-1z"
        role="button"
        data-slide="next"
      >
        <span
          className="moveControl carousel-control-next-icon"
          aria-hidden="true"
        ></span>
        <span className="sr-only">Next</span>
      </div>
    </div>
  );
}
