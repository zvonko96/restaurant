import React, { Component } from "react";
import { foodData } from "../data/food";
import Food from "./Food";

class FoodList extends Component {
  state = {
    foods: foodData
  };

  render() {
    return (
      <div className="row m-0">
        {this.state.foods.map(food => {
          return <Food key={food.id} foods={food} />;
        })}
      </div>
    );
  }
}

export default FoodList;
