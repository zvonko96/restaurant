import React from "react";
import Carousel from "./Carousel";
import "../css/Home.css";

export default function Home() {
  return (
    <div className="homePage">
      <Carousel />
      <h1>O nama</h1>
      <p>
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book. It has survived not only five
        centuries, but also the leap into electronic typesetting, remaining
        essentially unchanged. It was popularised in the 1960s with the release
        of Letraset sheets containing Lorem Ipsum passages, and more recently
        with desktop publishing software like Aldus PageMaker including versions
        of Lorem Ipsum.
      </p>
      <div className="row mobileContent">
        <div className="col-xs-12 col-sm-6">
          <h3>What is Lorem Ipsum?</h3>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </p>
        </div>
        <div className="col-xs-12 col-sm-6">
          <div className="row galeryFood">
            <div className="col-xs-12 col-sm-6 galeryPadd">
              <img
                src="https://krov.rs/wp-content/uploads/2018/08/rostilj-uvodna-800x445.jpg"
                alt="rostilj"
              />
            </div>
            <div className="col-xs-12 col-sm-6 galeryPadd">
              <img
                src="https://opusteno.rs/slike/2014/06/da-li-je-rostilj-kancerogen-22796/kancer-rostilj-meso-sp.jpg"
                alt="rostilj"
              />
            </div>
            <div className="col-xs-12 col-sm-6 galeryPadd">
              <img
                src="https://www.lepaisrecna.rs/files/2019/06/ako_spremate_rostilj_trik_sa_krompirom_morate_znati_evo_kako_da_vam_se_meso_i_riba_ne_lepe_na_grilu_video_630044472.jpg"
                alt="rostilj"
              />
            </div>
            <div className="col-xs-12 col-sm-6 galeryPadd">
              <img
                src="https://www.lepaisrecna.rs/files/2017/04/sve_za_rostilj_najbolji_recepti_i_saveti_za_grilovano_povrce_i_meso_na_zaru_foto_835533468.jpg"
                alt="rostilj"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="row mobileContent">
        <div className="col-xs-12 col-sm-6">
          <div className="row galeryFood">
            <div className="col-xs-12 col-sm-6 galeryPadd">
              <img
                src="https://www.jocooks.com/wp-content/uploads/2012/03/sex-in-a-pan-1-4-1-480x270.jpg"
                alt="dezert"
              />
            </div>
            <div className="col-xs-12 col-sm-6 galeryPadd">
              <img
                src="https://img.taste.com.au/LTmNoesH/w506-h253-cfill/taste/2016/11/easy-molten-chocolate-souffle-117417-1.jpg"
                alt="dezert"
              />
            </div>
            <div className="col-xs-12 col-sm-6 galeryPadd">
              <img src="https://ichef.bbci.co.uk/food/ic/food_16x9_448/recipes/meltedchocolatefonda_77527_16x9.jpg" />
            </div>
            <div className="col-xs-12 col-sm-6 galeryPadd">
              <img
                src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/smores-bake-1521234612.png?crop=0.752xw:0.670xh;0.248xw,0.174xh&resize=480:*"
                alt="dezert"
              />
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-sm-6">
          <h3>What is Lorem Ipsum?</h3>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </p>
        </div>
      </div>
    </div>
  );
}
