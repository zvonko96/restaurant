import React, { Component } from "react";
import Home from "./componenets/Home";
import Navbar from "./componenets/Navbar";
import "./App.css";
import Drink from "./componenets/Drink";
import FoodList from "./componenets/FoodList";
import { Route, Switch } from "react-router-dom";
import Default from "./componenets/Error";
import Footer from "./componenets/Footer";

class App extends Component {
  state = {};
  render() {
    return (
      <div className="container">
        <Navbar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/drink" component={Drink} />
          <Route path="/foodList" component={FoodList} />
          <Route component={Default} />
        </Switch>
        <Footer />
      </div>
    );
  }
}

export default App;
